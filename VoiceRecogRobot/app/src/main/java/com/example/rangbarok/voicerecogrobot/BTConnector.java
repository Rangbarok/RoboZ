package com.example.rangbarok.voicerecogrobot;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.UUID;

public class BTConnector {

    public BluetoothAdapter localAdapter;
    public BluetoothDevice nxt;
    public BluetoothSocket socket_nxt;
    public BufferedWriter outBT;

    public void enableBT(){
        localAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void connectToNXT(){
        BluetoothAdapter localAdapter;
        localAdapter = BluetoothAdapter.getDefaultAdapter();

         nxt = localAdapter.getRemoteDevice("00:16:53:0A:A5:7D");

         socket_nxt = null;

        try {
            socket_nxt = nxt.createRfcommSocketToServiceRecord(UUID
                    .fromString("00001101-0000-1000-8000-00805F9B34FB"));
            socket_nxt.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeMessage(int msg) throws InterruptedException {

        BluetoothSocket connSock;

        connSock = socket_nxt;

        if (connSock!=null) {
            try {

                DataOutputStream out = new DataOutputStream(connSock.getOutputStream());
                out.writeInt(msg);
                out.flush();

                Thread.sleep(1000);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int readMessage(String nxt) {

        BluetoothSocket connSock;
        connSock = socket_nxt;
        int n;

        if (connSock!=null) {
            try {
                /*DataInputStream in = new DataInputStream(connSock.getInputStream());
                in.read();*/

                InputStreamReader in = new InputStreamReader(connSock.getInputStream());
                n = in.read();
                return n;

            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            return -1;
        }
    }
}

