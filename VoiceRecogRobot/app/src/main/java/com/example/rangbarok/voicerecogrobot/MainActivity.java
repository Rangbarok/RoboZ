package com.example.rangbarok.voicerecogrobot;

import android.Manifest;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognitionService;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.rangbarok.voicerecogrobot.R;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private final String ACTION_DISPARAR= "dispara";
    private final String ACTION_DESACTIVAR = "desactivar";
    private final String ACTION_AVANZAR = "avanza";
    private final String ACTION_DERECHA = "derecha";
    private final String ACTION_IZQUIERDA = "izquierda";
    private final String ACTION_APAGAR = "apagar";
    private final String ACTION_PARAR = "para";

    private final int ACTION_AVANZAR_NXT = 0;
    private final int ACTION_DISPARAR_NXT = 1;
    private final int  ACTION_DERECHA_NXT = 2;
    private final int ACTION_IZQUIERDA_NXT = 3;
    private final int ACTION_APAGAR_NXT = 4;
    private final int ACTION_PARAR_NXT = 5;

    private final String ACTION_DISPARAR_MESSAGE = " is shooting!!";
    private final String ACTION_AVANZAR_MESSAGE = " is walking!!";
    private final String ACTION_DESACTIVAR_MESSAGE = " is desactivatied!!";
    private final String ACTION_DERECHA_MESSAGE = " is turn to right!!";
    private final String ACTION_IZQUIERDA_MESSAGE = " is turn to left!!";
    private final String ACTION_DEFAULT_MESSAGE = " doesn't support this command";
    private final String ACTION_NOMESSAGE_MESSAGE = "Nothing detected";
    private final String ACTION_APAGAR_MESSAGE = " is turn off!!";
    private final String ACTION_PARAR_MESSAGE = "";

    private final String ROBOT_NAME = "perro viejo";

    private String auxString;
    private Boolean robotActivated = false;

    private TextView log;
    private ImageButton btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermission();
        //desactivateSounds();

        log = (TextView) findViewById(R.id.log);
        btn = (ImageButton) findViewById(R.id.button);

        final SpeechRecognizer mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);

        final Intent mSpeechRecognizerIntent = new Intent (RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());

        final BTConnector btConnector = new BTConnector();
        btConnector.enableBT();
        btConnector.connectToNXT();


        Log.e("Error",""+!SpeechRecognizer.isRecognitionAvailable(this));

        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {

            @Override
            public void onReadyForSpeech(Bundle bundle) { }

            @Override
            public void onBeginningOfSpeech() { }

            @Override
            public void onRmsChanged(float v) { }

            @Override
            public void onBufferReceived(byte[] bytes) { }

            @Override
            public void onEndOfSpeech() {
                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
            }

            @Override
            public void onError(int i) {
                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
            }

            @Override
            public void onResults(Bundle bundle) {
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                for(String str: matches) {
                    if(str.length() < 20 && str.split(" ").length < 3) {
                        auxString = str;
                        auxString = auxString.trim().toLowerCase();
                        auxString = removeWord(auxString, ROBOT_NAME);
                        Boolean exit = false;

                        Log.e("Result", "" + matches.toString());

                        if (str.contains(ROBOT_NAME) && !robotActivated) {
                            btn.setImageResource(R.drawable.nomuted);
                            log.setText(ROBOT_NAME + " activated");
                            robotActivated = true;
                            exit = true;
                        } else if (robotActivated) {
                            switch (auxString) {
                                case ACTION_AVANZAR:
                                    log.setText("" + ROBOT_NAME + ACTION_AVANZAR_MESSAGE);
                                    exit = true;
                                    try {
                                        btConnector.writeMessage(ACTION_AVANZAR_NXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case ACTION_DISPARAR:
                                    log.setText("" + ROBOT_NAME + ACTION_DISPARAR_MESSAGE);
                                    exit = true;
                                    try {
                                        btConnector.writeMessage(ACTION_DISPARAR_NXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case ACTION_DESACTIVAR:
                                    log.setText("" + ROBOT_NAME + ACTION_DESACTIVAR_MESSAGE);
                                    btn.setImageResource(R.drawable.muted);
                                    robotActivated = false;
                                    exit = true;
                                    break;
                                case ACTION_DERECHA:
                                    log.setText("" + ROBOT_NAME + ACTION_DERECHA_MESSAGE);
                                    exit = true;
                                    try {
                                        btConnector.writeMessage(ACTION_DERECHA_NXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case ACTION_IZQUIERDA:
                                    log.setText("" + ROBOT_NAME + ACTION_IZQUIERDA_MESSAGE);
                                    exit = true;
                                    try {
                                        btConnector.writeMessage(ACTION_IZQUIERDA_NXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case ACTION_APAGAR:
                                    log.setText(""+ROBOT_NAME+ACTION_APAGAR_MESSAGE);
                                    exit = true;
                                    mSpeechRecognizer.stopListening();
                                    finish();
                                    try {
                                        btConnector.writeMessage(ACTION_APAGAR_NXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case ACTION_PARAR:
                                    log.setText(""+ROBOT_NAME+ACTION_PARAR_MESSAGE);
                                    exit = true;
                                    try {
                                        btConnector.writeMessage(ACTION_PARAR_NXT);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                default:
                                    log.setText(ROBOT_NAME + ACTION_DEFAULT_MESSAGE);
                                    break;
                            }
                        } else {
                            log.setText(ACTION_NOMESSAGE_MESSAGE);
                        }

                        if (exit)
                            break;
                    }
                }
            }

            @Override
            public void onPartialResults(Bundle bundle) { }

            @Override
            public void onEvent(int i, Bundle bundle) { }
        });


        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                finish();
            }
        }
    }

    public String removeWord(String sentence, String word) {
        if (sentence.contains(word))
            return sentence.replaceAll(word, "");
        return sentence;
    }

    /*public void desactivateSounds() {
        AudioManager aManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        aManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
        aManager.setStreamMute(AudioManager.STREAM_ALARM, false);
        aManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        aManager.setStreamMute(AudioManager.STREAM_RING, false);
        aManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
    }*/
}