package es.usj.robotica;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import lejos.nxt.LCD;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

@SuppressWarnings("unused")
public class BTConnection {
	
	public static final int ACTION_AVANZAR_NXT = 0;
	public static final int ACTION_DISPARAR_NXT = 1;
	
	public static final int ACTION_DERECHA_NXT = 2;
    public static final int ACTION_IZQUIERDA_NXT = 3;
    
    public static final int ACTION_PARAR_NXT = 5;
    
    public static final int ACTION_APAGAR_NXT = 4;
	
	
	private InputStreamReader in;
	private DataOutputStream out;
	
	private int message;
	private boolean messageReceived;
	
	public BTConnection() {
		NXTConnection btc = null;
		
		LCD.clear(); LCD.drawString("[BT]: Waiting...", 0, 0);
		
		btc = Bluetooth.waitForConnection();
		btc.setIOMode(NXTConnection.RAW);
		
		LCD.clear(); LCD.drawString("[BT]: Connected!", 0, 0);
		try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
		
		
		in = new InputStreamReader(btc.openInputStream());
		out = new DataOutputStream(btc.openOutputStream());
		
		message = -1;
		messageReceived = false;
	}
	
	@SuppressWarnings("restriction")
	public void read() {
		setMessageReceived(false);
		try {
			message = in.read();
			if (message != -1) setMessageReceived(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void show() {
		switch(message) {
			case ACTION_AVANZAR_NXT:
				LCD.clear(); LCD.drawString("Cmd: " + "AVANZAR", 0, 0);
				break;
			case ACTION_DISPARAR_NXT:
				LCD.clear(); LCD.drawString("Cmd: " + "DISPARAR", 0, 0);
				break;
			case ACTION_DERECHA_NXT:
				LCD.clear(); LCD.drawString("Cmd: " + "DERECHA", 0, 0);
				break;
			case ACTION_IZQUIERDA_NXT:
				LCD.clear(); LCD.drawString("Cmd: " + "IZQUIERDA", 0, 0);
				break;
			case ACTION_PARAR_NXT:
				LCD.clear(); LCD.drawString("Cmd: " + "PARAR", 0, 0);
				break;
			case ACTION_APAGAR_NXT:
				LCD.clear();
				break;
		}
	}
	
	public int getMessage() {
		return message;
	}
	
	public void setMessageReceived(boolean messageReceived) {
		this.messageReceived = messageReceived;
	}

	public boolean isMessageReceived() {
		return messageReceived;
	}
	
	
	
}
