package es.usj.robotica;

import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

class UltraSonicSensor implements Runnable {
	private Thread t;
	
	private UltrasonicSensor us;
	
	public UltraSonicSensor() {
	    t = new Thread(this, "UltrasonicSensor");
	    t.start();
	    
	    us = new UltrasonicSensor(SensorPort.S4);
	}
	
	public void run() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		while (true) {
			if (us.getDistance() < 30) {
				Motor.A.stop();
				Motor.C.stop();
				return;
			}
		}
	}
}

public class RoboZ implements Runnable {
	private boolean running;
	private Thread thread;
	
	private BTConnection btc;
	
	private UltraSonicSensor us;
	
	public RoboZ() {
		init();
	}
	
	public synchronized void start() {
		if (running) return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public synchronized void stop() {
		if (!running) return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void init() {
		btc = new BTConnection();
	}

	public void run() {
		LCD.clear(); LCD.drawString("RoboZ | v. 0.0.1", 0, 0);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		while (running) {
			btc.read();
			if (btc.isMessageReceived()) {
				if (us != null) us = null;
				btc.show();
				switch(btc.getMessage()) {
					case BTConnection.ACTION_DISPARAR_NXT:
						Motor.A.stop();
						Motor.C.stop();
						break;
					case BTConnection.ACTION_AVANZAR_NXT:
						Motor.A.setSpeed(720);
						Motor.C.setSpeed(720);
						Motor.A.forward();
						Motor.C.forward();
						if (us == null) us = new UltraSonicSensor();
						break;
					case BTConnection.ACTION_DERECHA_NXT:
						Motor.C.stop();
						if (Motor.A.isStalled()) {
							Motor.A.setSpeed(360);
							Motor.A.forward();
						}
						try {
							Thread.sleep(500);
							Motor.A.setSpeed(720);
							Motor.C.setSpeed(720);
							Motor.A.forward();
							Motor.C.forward();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (us == null) us = new UltraSonicSensor();
						break;
					case BTConnection.ACTION_IZQUIERDA_NXT:
						Motor.A.stop();
						if (Motor.C.isStalled()) {
							Motor.C.setSpeed(360);
							Motor.C.forward(); 
						}
						try {
							Thread.sleep(500);
							Motor.A.setSpeed(720);
							Motor.C.setSpeed(720);
							Motor.A.forward();
							Motor.C.forward();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (us == null) us = new UltraSonicSensor();
						break;
					case BTConnection.ACTION_PARAR_NXT:
						Motor.A.stop();
						Motor.C.stop();
						break;
					case BTConnection.ACTION_APAGAR_NXT:
						running = false;
						break;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		RoboZ roboz = new RoboZ();
		roboz.start();
	}

}
