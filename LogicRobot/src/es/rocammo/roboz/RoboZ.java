package es.rocammo.roboz;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.Sound;

@SuppressWarnings("unused")
public class RoboZ implements Runnable {
	
	private boolean running;
	private Thread thread;
	
	private BTConnection btc;
	private File snd_file;
	
	public RoboZ() {
		init();
	}
	
	public synchronized void start() {
		if (running) return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public synchronized void stop() {
		if (!running) return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void init() {
		btc = new BTConnection();
		
		snd_file = new File("cartoon043.wav");
	}

	public void run() {
		LCD.clear(); LCD.drawString("RoboZ | v. 0.0.1", 0, 0);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		while (running) {
			btc.read();
			if (btc.isMessageReceived()) {
				btc.show();
				switch(btc.getMessage()) {
				case BTConnection.ACTION_DISPARAR_NXT:
					Motor.A.stop(); Motor.C.stop();;
					Sound.playSample(snd_file, Sound.VOL_MAX);
					break;
				case BTConnection.ACTION_AVANZAR_NXT:
					Motor.A.setSpeed(720);
					Motor.C.setSpeed(720);
					Motor.A.forward();
					Motor.C.forward();
					break;
				case BTConnection.ACTION_DERECHA_NXT:
					Motor.C.stop();
					if (Motor.A.isStalled()) {
						Motor.A.setSpeed(360);
						Motor.A.forward();
					}

					try { Thread.sleep(500); Motor.A.setSpeed(720);
					Motor.C.setSpeed(720);
					Motor.A.forward();
					Motor.C.forward();} catch (InterruptedException e) { e.printStackTrace(); }
					break;
				case BTConnection.ACTION_IZQUIERDA_NXT:
					Motor.A.stop();
					if (Motor.C.isStalled()) {
						Motor.C.setSpeed(360);
						Motor.C.forward(); 
					}
					try { Thread.sleep(500); Motor.A.setSpeed(720);
					Motor.C.setSpeed(720);
					Motor.A.forward();
					Motor.C.forward();} catch (InterruptedException e) { e.printStackTrace(); }
					break;
				case BTConnection.ACTION_PARAR_NXT:
					Motor.A.stop();
					Motor.C.stop();
				}
				
				if (btc.getMessage() == BTConnection.ACTION_APAGAR_NXT) break;
			}
		}
	}
	
	public static void main(String[] args) {
		RoboZ roboz = new RoboZ();
		roboz.start();
	}

}
